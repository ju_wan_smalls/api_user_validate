SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `smalls_config`;
CREATE TABLE `smalls_config` (
  `skey` varchar(32) NOT NULL DEFAULT '',
  `value` text,
  `remarks` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`skey`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
INSERT INTO `smalls_config` VALUES ('admin_pwd', 'cd069b5da266ad7acfd138a342368d76', '管理员密码');
INSERT INTO `smalls_config` VALUES ('admin_user', 'smalls', '管理员账号');
INSERT INTO `smalls_config` VALUES ('admin_halt', '#smalls#@', '加密盐');
INSERT INTO `smalls_config` VALUES ('update_url', 'http://baidu.com', null);
INSERT INTO `smalls_config` VALUES ('down_state', '1', null);
INSERT INTO `smalls_config` VALUES ('update_state', '1', null);
INSERT INTO `smalls_config` VALUES ('app_gonggao', '加密算法正在更新', null);
INSERT INTO `smalls_config` VALUES ('kefu_qq', '213123333', null);
INSERT INTO `smalls_config` VALUES ('qun_key', '3123123', null);
INSERT INTO `smalls_config` VALUES ('kami_carry_url', 'http://smallsadmin.cn', null);
INSERT INTO `smalls_config` VALUES ('kami_query_url', 'http://smallsadmin.cn', null);
INSERT INTO `smalls_config` VALUES ('app_state', '1', null);
INSERT INTO `smalls_config` VALUES ('kami_state', '1', null);
INSERT INTO `smalls_config` VALUES ('gonggao_state', '1', null);
INSERT INTO `smalls_config` VALUES ('gonggao', '13213123', null);
INSERT INTO `smalls_config` VALUES ('submit', 'ok', null);
INSERT INTO `smalls_config` VALUES ('update_ver', '1', null);
INSERT INTO `smalls_config` VALUES ('update_desc', '新版更新大量内容', null);
DROP TABLE IF EXISTS `smalls_kami`;
CREATE TABLE `smalls_kami` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kami` varchar(64) DEFAULT NULL,
  `type` tinyint(1) DEFAULT '0',
  `userid` int(10) DEFAULT NULL,
  `remarks` varchar(20) DEFAULT NULL,
  `end_time` int(10) DEFAULT '0',
  `add_time` int(10) DEFAULT '1',
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `smalls_user`;
CREATE TABLE `smalls_user` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `pic` varchar(255) DEFAULT NULL,
  `nickname` varchar(155) DEFAULT NULL,
  `phone` bigint(13) DEFAULT NULL,
  `token` varchar(32) DEFAULT NULL,
  `is_vip` tinyint(1) DEFAULT '0',
  `qq` bigint(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `update_time` int(11) DEFAULT NULL,
  `add_time` int(11) DEFAULT NULL,
  `ip` varchar(32) DEFAULT NULL,
  `mark` varchar(32) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=MyISAM AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8;