<?php
/**
 * Created by Smalls.
 * User: Smalls
 * Email: admin@iiitool.com
 * QQ:13242463,支持定制
 * Date: 2019/1/6
 * Time: 16:17
 */
$config = [
    'kami_type'=>[
        0=>'全场通用卡密',
        1=>'飞车会员卡密',
    ],
    'user_type'=>[
       1=>'QQ飞车',
       2=>'王者荣耀',
       3=>'其他',
    ],
];