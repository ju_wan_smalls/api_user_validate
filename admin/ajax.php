<?php
/**
 * Created by Smalls.
 * User: Smalls
 * Email: admin@iiitool.com
 * QQ:13242463,支持定制
 * Date: 2019/1/5
 * Time: 18:17
 */
include("../includes/common.php");
if($islogin==1){}else exit("<script language='javascript'>window.location.href='./login.php';</script>");

if(isset($_GET['act']) && isset($_GET['mod'])){
    if($_GET['mod']=='kami'){
        if($_GET['act']=='has_del'){
            $status = 1;
            $sql="DELETE FROM smalls_kami WHERE status='$status'";
            $row=$DB->exec($sql);
            if($row){
                show(1,'删除成功',[]);
            }else{
                show(0,'删除失败',[]);
            }
        }elseif($_GET['act']=='all_del'){
            $sql="DELETE FROM smalls_kami WHERE 1";
            $row=$DB->exec($sql);
            if($row){
                show(1,'删除成功',[]);
            }else{
                show(0,'删除失败',[]);
            }
        }elseif($_GET['act']=='sp_plexe'){
            $str = daddslashes($_POST['str'])?daddslashes($_POST['str']):1;
            $type = daddslashes($_POST['type'])?daddslashes($_POST['type']):1;
            $str = explode(',',$str);
            if($type==1){
                if($str){
                    foreach ($str as $v){
                        $sql="DELETE FROM smalls_kami WHERE id='$v'";
                        $row=$DB->exec($sql);
                    }
                    show(1,'删除成功',[]);
                }
                show(0,'删除失败',[]);
            }else{
                show(0,'未知内容',[]);
            }
        }elseif($_GET['act']=='abstract'){
            $type = daddslashes($_POST['type'])?daddslashes($_POST['type']):show(0,'请输入类型',[]);
            if($type==1){
                $time = time()-600;
                $rows=$DB->query("select * from smalls_kami where `add_time`>'$time' order by id desc")->fetchAll();
            }elseif($type==2){
                $time = time()-3600;
                $rows=$DB->query("select * from smalls_kami where `add_time`>'$time' order by id desc")->fetchAll();
            }elseif($type==3){
                $rows=$DB->query("select * from smalls_kami where `status`=0 order by id desc")->fetchAll();
            }
                if($rows){
                    //var_dump($rows);die;
                    $list = [];
                    foreach ($rows as $id => $v){
                        $list[$id] = $v['kami'];
                    }
                    show(1,'获取成功',$list);
                }else{
                    show(0,'获取失败',[]);
                }
        }elseif($_GET['act']=='del'){
            $id = daddslashes($_POST['id'])?daddslashes($_POST['id']):show(0,'请输入id',[]);
            $sql="DELETE FROM smalls_kami WHERE id='$id'";
            $row=$DB->exec($sql);
            if($row){
                show(1,'删除成功',[]);
            }else{
                show(0,'删除失败',[]);
            }
        }
    }elseif($_GET['mod']=='user'){
        if($_GET['act']=='status'){
            $id = daddslashes($_POST['id'])?daddslashes($_POST['id']):show(0,'请输入id',[]);
            $rows=$DB->query("select * from smalls_user where userid='$id' limit 1")->fetch();
            if($rows){
                if($rows['status']==1){
                    $status = 0;
                }else{
                    $status = 1;
                }
                $sql="update `smalls_user` set `status` ='{$status}' where `userid`='$id'";
                $row=$DB->exec($sql);
                if($row){
                    show(1,'修改成功',[]);
                }else{
                    show(0,'修改失败',[]);
                }
            }else{
                show(0,'修改失败',[]);
            }
        }elseif($_GET['act']=='editvip'){
            $id = daddslashes($_POST['id'])?daddslashes($_POST['id']):show(0,'请输入id',[]);
            $rows=$DB->query("select * from smalls_user where userid='$id' limit 1")->fetch();
            if($rows){
                if($rows['is_vip']==1){
                    $is_vip = 0;
                }else{
                    $is_vip = 1;
                }
                $sql="update `smalls_user` set `is_vip` ='{$is_vip}' where `userid`='$id'";
                $row=$DB->exec($sql);
                if($row){
                    show(1,'修改成功',[]);
                }else{
                    show(0,'修改失败',[]);
                }
            }else{
                show(0,'修改失败',[]);
            }
        }elseif($_GET['act']=='info'){
            $id = daddslashes($_POST['id'])?daddslashes($_POST['id']):show(0,'请输入id',[]);
            $rows=$DB->query("select * from smalls_user where userid='$id' limit 1")->fetch();
            if($rows){
                foreach ($rows as $id => $v){
                    if(is_numeric($id)){
                        unset($rows[$id]);
                    }
                    if($id=='update_time' || $id=='add_time'){
                        $rows[$id] = date('Y-m-d H:i:s', $rows[$id]);
                    }
                }
                show(1,'修改成功',$rows);
            }else{
                show(0,'修改失败',[]);
            }
        }
    }
}