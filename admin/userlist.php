<?php
/**
 * Created by Smalls.
 * User: Smalls
 * Email: admin@iiitool.com
 * QQ:13242463,支持定制
 * Date: 2019/1/5
 * Time: 22:14
 */
include("../includes/common.php");
if($islogin==1){}else exit("<script language='javascript'>window.location.href='./login.php';</script>");
$title=' 用户列表';
$css = '	<style type="text/css">
		.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
			padding: 15px;
		}
	</style>
';
include './common/header.php';
//该文件全局变量
$sql=" 1";
$pagesize=PAGE;
$urlfile = 'userlist.php';
$all_count=$DB->query("SELECT * from smalls_user WHERE {$sql}")->rowCount();
$vip_count=$DB->query("SELECT * from smalls_user WHERE `is_vip`=1")->rowCount();
$status_count=$DB->query("SELECT * from smalls_user WHERE `status`=0")->rowCount();
if(isset($_GET['act'])){
    if ($_GET['act']=='search'){
        $content = daddslashes($_GET['content']);
        $str = daddslashes($_GET['str']);
        $type = daddslashes($_GET['type']);
        if($type>0)
            $sql=" `type`='{$type}' AND";
        else
            $sql='';
        if($str==1)
            $sql .= " `userid`='{$content}'";
        elseif($str==2)
            $sql .= " `qq`='{$content}'";
        elseif($str==3)
            $sql .= " `phone`='{$content}'";
        elseif($str==4)
            $sql .= " `token`='{$content}'";
        elseif($str==5)
            $sql .= " `username`='{$content}'";
        else
            $sql=' 1';
    }
}
$gls=$DB->query("SELECT * from smalls_user WHERE {$sql}")->rowCount();
?>
<div class="admin-wrap">
    <div class="container">
        <div class="row admin-row">
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                            <tr height="25">
                                <td align="center">
                                    <font color="#808080">
                                        <b><span class="glyphicon glyphicon-flag">总用户</b></br><?php echo $all_count;?></font>
                                </td>
                                <td align="center">
                                    <font color="#808080">
                                        <b><span class="glyphicon glyphicon-plus-sign"></span>会员用户</b><br><?php echo $vip_count;?></font>
                                </td>
                                <td align="center">
                                    <font color="#808080">
                                        <b><span class="glyphicon glyphicon-ok"></span>封号用户</b><br><?php echo $status_count;?></font>
                                </td>
                                <td align="center">
                                    <font color="#808080">
                                        <a class="btn btn-info" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-search"></span>搜索用户</a>
                                    </font>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>UserId</th>
                                <th>用户昵称</th>
                                <th>所属类型</th>
                                <th>类型</th>
                                <th>Ip</th>
                                <th>添加时间</th>
                                <th>状态(点击封号)</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                $pages=intval($numrows/$pagesize);
                                if ($numrows%$pagesize)
                                {
                                    $pages++;
                                }
                                if (isset($_GET['page'])){
                                    $page=intval($_GET['page']);
                                }
                                else{
                                    $page=1;
                                }
                                $offset=$pagesize*($page - 1);

                                $rs=$DB->query("SELECT * FROM smalls_user WHERE{$sql} order by userid desc limit $offset,$pagesize");
                                while($res = $rs->fetch())
                                {
                                    if($res['status']==1)
                                        $status = '<a onclick="Status('.$res['userid'].')" title="点我修改用户状态"><font class="btn btn-xs btn-info">正常</font></a>';
                                    else
                                        $status = '<a onclick="Status('.$res['userid'].')" title="点我修改用户状态"><font class="btn btn-xs btn-danger">封号中</font></a>';
                                    if($res['is_vip']==0)
                                        $is_vip = '<a onclick="editvip('.$res['userid'].')" title="点我修改会员状态">非会员</a>';
                                    else
                                        $is_vip = '<a onclick="editvip('.$res['userid'].')" title="点我修改会员状态"><font color=red>会员用户</font></a>';
                                    if(isset($config['user_type'][$res['type']]))
                                        $res['type'] = $config['user_type'][$res['type']];
                                    else
                                        $res['type'] = '其他';
                                    echo '<tr>
                                <td>'.$res['userid'].'</td>
                                <td>'.$res['nickname'].'</td>
                                <td>'.$res['type'].'</td>
                                <td>'.$is_vip.'</td>
                                <td>'.$res['ip'].'</td>
                                <td>'.date('Y-m-d H:i:s', $res['add_time']).'</td>
                                <td>'.$status.'</td>
                                <td><a onclick="info('.$res['userid'].')" class="btn btn-warning btn-xs">信息</a></td> </tr>';
                                }
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php
            echo'<ul class="pagination">';
            $s = ceil($gls / $pagesize);
            $first=1;
            $prev=$page-1;
            $next=$page+1;
            $last=$s;
            if ($page>1)
            {
                echo '<li><a href="'.$urlfile.'?page='.$first.$link.'">«</a></li>';
                echo '<li><a href="'.$urlfile.'?page='.$prev.$link.'">&laquo;</a></li>';
            } else {
                echo '<li class="disabled"><a>«</a></li>';
                echo '<li class="disabled"><a>&laquo;</a></li>';
            }
            for ($i=1;$i<$page;$i++)
                echo '<li><a href="'.$urlfile.'?page='.$i.$link.'">'.$i .'</a></li>';
            echo '<li class="disabled"><a>'.$page.'</a></li>';
            for ($i=$page+1;$i<=$s;$i++)
                echo '<li><a href="'.$urlfile.'?page='.$i.$link.'">'.$i .'</a></li>';
            echo '';
            if ($page<$s)
            {
                echo '<li><a href="'.$urlfile.'?page='.$next.$link.'">&raquo;</a></li>';
                echo '<li><a href="'.$urlfile.'?page='.$last.$link.'">»</a></li>';
            } else {
                echo '<li class="disabled"><a>&raquo;</a></li>';
                echo '<li class="disabled"><a>»</a></li>';
            }
            echo'</ul>';
            #分页
            ?>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="?act=search" method="GET">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">搜索记录</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="act" value="search">
                    <select class="form-control" name="type">
                        <option value="0">全部类型</option>
                        <?php
                        if(isset($config['user_type'])){
                            if($config['user_type']){
                                foreach ($config['user_type'] as $id => $v){
                                    $v = daddslashes($v);
                                    if(daddslashes($_GET['type'])==$id){
                                        $s = 'selected';
                                    }else{
                                        $s ='';
                                    }
                                    echo '<option value="'.$id.'" '.$s.'>'.$v.'</option>';
                                }
                            }
                        }
                        ?>
                    </select><br>
                    <select class="form-control" name="str">
                        <option value="1" <?php if(daddslashes($_GET['str'])==1)echo 'selected' ?>>使用用户ID查询</option>
                        <option value="2" <?php if(daddslashes($_GET['str'])==2)echo 'selected' ?>>使用QQ查询</option>
                        <option value="3" <?php if(daddslashes($_GET['str'])==3)echo 'selected' ?>>使用手机查询</option>
                        <option value="4" <?php if(daddslashes($_GET['str'])==4)echo 'selected' ?>>使用唯一标识符查询</option>
                        <option value="5" <?php if(daddslashes($_GET['str'])==5)echo 'selected' ?>>使用用户名查询</option>
                    </select><br>
                    <input type="text" class="form-control" name="content" placeholder="用户ID/QQ/手机/唯一标识符/用户名！跟上面所对应" value="<?php echo daddslashes($_GET['content']); ?>">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="submit" class="btn btn-primary">立即搜索</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    function Status(id){
        var index = layer.msg('加载中', {icon: 16,shade: 0.01});
        $.ajax({
            type: "POST",
            url: "ajax.php?mod=user&act=status",
            data: {id:id},
            dataType: "json",
            success: function(data){
                layer.close(index);
                if(data.code==1){
                    window.location.reload();
                }else{
                    layer.msg(data.msg, {icon: 5});
                    return false;
                }
            },
            error: function () {
                layer.close(index);
                layer.msg('服务器升级中！');
                return false;
            }
        })
    }
    function editvip(id){
        var index = layer.msg('加载中', {icon: 16,shade: 0.01});
        $.ajax({
            type: "POST",
            url: "ajax.php?mod=user&act=editvip",
            data: {id:id},
            dataType: "json",
            success: function(data){
                layer.close(index);
                if(data.code==1){
                    window.location.reload();
                }else{
                    layer.msg(data.msg, {icon: 5});
                    return false;
                }
            },
            error: function () {
                layer.close(index);
                layer.msg('服务器升级中！');
                return false;
            }
        })
    }
    function info(id){
        var html ="";
        var ii = layer.load(2, {shade:[0.1,'#fff']});
        $.ajax({
            type : "POST",
            url : "ajax.php?mod=user&act=info",
            data : {"id":id},
            dataType : 'json',
            success : function(data) {
                layer.close(ii);
                if(data.code == 1){
                    var datamsg = data.data;
                    html = '<ul class="list-group">'+
                        '  <li class="list-group-item">用户ID：'+datamsg.userid+'</li>'+
                        '  <li class="list-group-item">用户名：'+datamsg.username+'</li>'+
                        '  <li class="list-group-item">用户昵称：'+datamsg.nickname+'</li>'+
                        '  <li class="list-group-item">QQ号：'+datamsg.qq+'</li>'+
                        '  <li class="list-group-item">手机号：'+datamsg.phone+'</li>'+
                        '  <li class="list-group-item">IP：'+datamsg.ip+'</li>'+
                        '  <li class="list-group-item">手机型号：'+datamsg.mark+'</li>'+
                        '  <li class="list-group-item">用户状态：'+(datamsg.status==0?"<font color=red>封号</font>":"<font color=green>正常</font>")+'</li>'+
                    '  <li class="list-group-item">会员信息：'+(datamsg.is_vip==0?"<font color=#ffebcd>非会员</font>":"<font color=red>会员用户</font>")+'</li>'+
                        '  <li class="list-group-item">更新时间：'+datamsg.update_time+'</li>'+
                        '  <li class="list-group-item">注册时间：'+datamsg.add_time+'</li>'+
                    '</ul>';
                    layer.open({
                        type: 1,
                        title:'用户详细信息',
                        skin: 'layui-layer-rim', //加上边框
                        area: ['380px', '450px'], //宽高
                        content: html
                    });

                }else{
                    layer.msg(data.msg);
                    return false;
                }
            },
            error:function(data){
                layer.close(ii);
                layer.msg('系统错误！');
                return false;
            }
        })


    }
</script>
</body>
</html>

