<?php
/**
 * Created by Smalls.
 * User: Smalls
 * Email: admin@iiitool.com
 * QQ:13242463,支持定制
 * Date: 2019/1/6
 * Time: 18:25
 */
include("../includes/common.php");
if($islogin==1){}else exit("<script language='javascript'>window.location.href='./login.php';</script>");
$title=' 修改密码';
$css = '	<style type="text/css">
		.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
			padding: 15px;
		}
	</style>';
include './common/header.php';
$urlfile = 'editpwd.php';
if(isset($_POST['editpwd'])) {
    @header('Content-Type: text/html; charset=UTF-8');
    $admin_user=daddslashes($_POST['admin_user']);
    $password=daddslashes($_POST['password']);
    if($admin_user==''){
        exit("<script language='javascript'>alert('请输入用户名！');window.location.href='./".$urlfile."';</script>");
    }else{
        if($password==''){
            $DB->query("update `smalls_config` set `value` ='$admin_user' where `skey`='admin_user'");
        }else{
            if(strlen($password)<7){
                exit("<script language='javascript'>alert('密码请大于6个字符！');window.location.href='./".$urlfile."';</script>");
            }
            $password = md5($password.$conf['admin_halt']);
            $DB->query("update `smalls_config` set `value` ='$password' where `skey`='admin_pwd'");
            $DB->query("update `smalls_config` set `value` ='$admin_user' where `skey`='admin_user'");
        }
    }
    exit("<script language='javascript'>alert('修改成功！');window.location.href='./".$urlfile."';</script>");
}
?>
<div class="admin-wrap">
    <div class="container">
        <div class="row admin-row">
            <div class="col-sm-offset-2 col-sm-8">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title">系统配置</h3></div>
                    <div class="panel-body">
                        <form action="" method="post" class="form-horizontal" role="form">
                            <h3>修改密码</h3><hr>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">用户名</label>
                                <div class="col-sm-9"><input type="text" name="admin_user" value="<?php echo $conf['admin_user'];?>" class="form-control" required="" placeholder="您的用户名"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">新密码</label>
                                <div class="col-sm-9"><input type="text" name="password" value="" class="form-control"  placeholder="不修改留空即可"></div>
                            </div>
                            <input type="hidden" name="editpwd" value="ok">
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-9">
                                    <input type="submit" value="修改保存" class="btn btn-block btn-primary">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
