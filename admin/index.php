<?php
/**
 * Created by Smalls.
 * User: Smalls
 * Email: admin@iiitool.com
 * QQ:13242463,支持定制
 * Date: 2019/1/5
 * Time: 18:06
 */
include("../includes/common.php");
if($islogin==1){}else exit("<script language='javascript'>window.location.href='./login.php';</script>");
$title=' 管理首页';
include './common/header.php';
?>
<div class="admin-wrap">
    <div class="container">
        <div class="row admin-row">
            <div class="col-xs-12 col-sm-12 col-lg-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        系统程序
                    </div>
                    <div class="panel-body">
                        <div class="alert alert-danger">
                            网站域名：<?php echo $_SERVER['HTTP_HOST'];?>
                        </div>
                        <div class="alert alert-success">
                            当前版本：V1.0 (Build 1000)
                        </div>
                        <div class="alert alert-info">
                            程序更新博客：<a href="http://blog.iiitool.com" target="_black">blog.iiitool.com</a> (小S博客)
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-lg-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        博客文章
                    </div>
                    <div class="panel-body">
                        <div class="list-group list-news-group">
                            <?php echo blog_list(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>