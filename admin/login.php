<?php
/**
 * Created by Smalls.
 * User: Smalls
 * Email: admin@iiitool.com
 * QQ:13242463,支持定制
 * Date: 2019/1/5
 * Time: 18:17
 */
include("../includes/common.php");
if(isset($_POST['user']) && isset($_POST['pass'])){
    $user=daddslashes($_POST['user']);
    $pass=daddslashes($_POST['pass']);
    $code=strtolower(daddslashes($_POST['code']));
    @header('Content-Type: text/html; charset=UTF-8');
    if($code!=$_SESSION["code_session"]){
        exit("<script language='javascript'>alert('验证码错误！');history.go(-1);</script>");
    }
    if($conf['admin_user']==$user && $conf['admin_pwd']==md5($pass.$conf['admin_halt'])){
        $session=md5($user.md5($pass.$conf['admin_halt']).$password_hash);
        $token=authcode("{$user}\t{$session}", 'ENCODE', SYS_KEY);
        setcookie("smalls_token", $token, time() + 604800);
        exit("<script language='javascript'>alert('后台登录平台成功！');window.location.href='./';</script>");
    }else{
        exit("<script language='javascript'>alert('用户名或密码不正确！');history.go(-1);</script>");
    }
}elseif(isset($_GET['logout'])){
    setcookie("smalls_token", "", time() - 604800);
    @header('Content-Type: text/html; charset=UTF-8');
    exit("<script language='javascript'>alert('您已成功注销本次登录！');window.location.href='./login.php';</script>");
}elseif($islogin==1){
    exit("<script language='javascript'>alert('您已登录！');window.location.href='./';</script>");
}
$title='用户登录';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title;?></title>
    <link rel="stylesheet" type="text/css" href="./assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="./assets/css/admin.css"/>
    <script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
    <script src="./assets/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
</head>
<style type="text/css">
    .input-group{
        margin-bottom: 15px;
    }
    .form-control{
        height: 37px;
    }
    .panel-body{
        padding: 20px 20px 30px 20px;
    }
    .panel-heading{
        text-align: center;
        padding:15px 20px;
    }
</style>
<body>
<div class="container" style="padding-top:70px;">
    <div class="col-xs-12 col-sm-10 col-md-8 col-lg-6 center-block" style="float: none;">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">管理员登录</h3></div>
            <div class="panel-body">
                <form action="" method="post" class="form-horizontal" role="form">
                    <div class="input-group">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                        <input type="text" name="user" class="form-control" placeholder="用户名" required="required"/>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                        <input type="password" name="pass" class="form-control" placeholder="密码" required="required"/>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-adjust"></span></span>
                        <input type="text" class="form-control" name="code" placeholder="输入验证码" autocomplete="off" required>
                        <span class="input-group-addon" style="padding: 0">
									<img src="./captcha.php" height="35" onclick="this.src='./captcha.php?'+Math.random();">
								</span>
                    </div>
                    <div class="form-button-group">
                        <input type="submit" value="登录" class="btn btn-block btn-primary"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
