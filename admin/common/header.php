<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title;?></title>
    <link rel="stylesheet" type="text/css" href="./assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="./assets/css/admin.css"/>
    <script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
    <script src="./assets/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="./assets/layer/layer.js" type="text/javascript" charset="utf-8"></script>
</head>
<?php echo $css;?>
<body>
<nav class="navbar navbar-fixed-top navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">导航按钮</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="./">后台管理中心</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right" >
                <li>
                    <a href="./index.php"><span class="glyphicon glyphicon-th-large"></span> 平台首页</a>
                </li>
                <li>
                    <a href="./kamilist.php"><span  class="glyphicon glyphicon-list-alt"></span> 卡密管理</a>
                </li>
                <li>
                    <a href="./userlist.php"><span  class="glyphicon glyphicon-user"></span> 用户管理</a>
                </li>
                <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" ><span class="glyphicon glyphicon-cog"></span> 系统设置<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="./app.php">APP配置</a></li>
                        <li><a href="./editpwd.php">修改密码</a><li>
                    </ul>
                </li>
                <li>
                    <a href="./login.php?logout"><span class="glyphicon glyphicon-log-out"></span> 退出登录</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
