<?php
/**
 * Created by Smalls.
 * User: Smalls
 * Email: admin@iiitool.com
 * QQ:13242463,支持定制
 * Date: 2019/1/5
 * Time: 22:14
 */
include("../includes/common.php");
if($islogin==1){}else exit("<script language='javascript'>window.location.href='./login.php';</script>");
$title=' 卡密列表';
$css = '	<style type="text/css">
		.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
			padding: 15px;
		}
	</style>
';
include './common/header.php';
//该文件全局变量
$sql=" 1";
$pagesize=PAGE;
$urlfile = 'kamilist.php';
$all_count=$DB->query("SELECT * from smalls_kami WHERE {$sql}")->rowCount();
$has_count=$DB->query("SELECT * from smalls_kami WHERE `status`=1")->rowCount();
if(isset($_GET['act'])){
    if($_GET['act']=='add'){
        $number = daddslashes($_POST['number'])?daddslashes($_POST['number']):1;
        $type = daddslashes($_POST['type'])?daddslashes($_POST['type']):0;
        $end_time = daddslashes($_POST['end_time'])?daddslashes($_POST['end_time']):date("Y-m-d H:i:s",strtotime("+1months",strtotime(date('Y-m-d H:i:s',time()))));
        $end_time = strtotime($end_time);
        $add_time = time();
        $remarks = daddslashes($_POST['remarks'])?daddslashes($_POST['remarks']):NULL;
        for($i=0;$i<$number;$i++){
            $kami = guid();
            $DB->exec("INSERT INTO `smalls_kami` (`kami`, `type`, `end_time`, `remarks`, `add_time`) VALUES ('{$kami}', '{$type}', '{$end_time}', '{$remarks}', '{$add_time}')");
        }
        @header('Content-Type: text/html; charset=UTF-8');
        exit("<script language='javascript'>alert('添加卡密成功！');window.location.href='./".$urlfile."';</script>");
    }elseif ($_GET['act']=='search'){
        $pz = daddslashes($_POST['pz']);
        if($pz==''){
            exit("<script language='javascript'>alert('您没有输入任何值！');window.location.href='./".$urlfile."';</script>");
        }
        if(is_numeric($pz)){
            $sql=" `userid`='{$pz}'";
        }else{
            $sql=" `kami`='{$pz}'";
        }
    }
}
$gls=$DB->query("SELECT * from smalls_kami WHERE {$sql}")->rowCount();
?>
<div class="admin-wrap">
    <div class="container">
        <div class="row admin-row">
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                            <tr height="25">
                                <td align="center">
                                    <font color="#808080"><b><span class="glyphicon glyphicon-hand-right"></span> 总卡密</b></br><?php echo $all_count;?></font>
                                </td>
                                <td align="center">
                                    <font color="#808080"><b><span class="glyphicon glyphicon-hand-right"></span> 已使用</b></br><?php echo $has_count;?></font>
                                </td>
                                <td align="center">
                                    <font color="#808080"><b><a data-toggle="modal" data-target="#myModal2" class="btn btn-success"><span class="glyphicon glyphicon-plus-sign"></span> 生成卡密</a>
                                    </font>
                                </td>
                                <td align="center">
                                    <font color="#808080"><b><a class="btn btn-info"  data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-search" ></span> 搜索</a>
                                    </font>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <center>
                            <div class="btn-group">
                                <button type="button" class="btn btn-warning" id="btn-det-ysykm">清除已使用卡密</button>
                                <button type="button" class="btn btn-danger" id="btn-det-allkm">清空全部卡密</button>
                                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#mykamiModal"">卡密一键提取</button>
                            </div>
                        </center>
                        <br>  <br>

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th><input type="checkbox" onclick="allcheck(this)" /></th>
                                <th>卡密劵</th>
                                <th>类型</th>
                                <th>使用者</th>
                                <th>备注</th>
                                <th>添加时间/到期时间</th>
                                <th>状态</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                $pages=intval($numrows/$pagesize);
                                if ($numrows%$pagesize)
                                {
                                    $pages++;
                                }
                                if (isset($_GET['page'])){
                                    $page=intval($_GET['page']);
                                }
                                else{
                                    $page=1;
                                }
                                $offset=$pagesize*($page - 1);

                                $rs=$DB->query("SELECT * FROM smalls_kami WHERE{$sql} order by id desc limit $offset,$pagesize");
                                while($res = $rs->fetch())
                                {
                                    if($res['type']==0)
                                        $type = '<font color=blue>'.$config['kami_type'][$res['type']].'</font>';
                                    else
                                        $type = '<font color=#663399>'.$config['kami_type'][$res['type']].'</font>';
                                    if($res['status']==0)
                                        $status = '<font color=green>未使用</font>';
                                    else
                                        $status = '<font color=red>已使用</font>';
                                    if($res['userid']=='')
                                        $userid = '未使用';
                                    else
                                        $userid = '使用者：'.$res['userid'];
                                    echo '<tr id="tr_'.$res['id'].'"><td><input type="checkbox" class="chkbox" ids="'.$res['id'].'" /></td>
                                <td>'.$res['kami'].'</td>
                                <td>'.$type.'</td>
                                <td>'.$userid.'</td>
                                <td>'.$res['remarks'].'</td>
                                <td>'.date('Y-m-d H:i:s', $res['add_time']).'/'.date('Y-m-d H:i:s', $res['end_time']).'</td>
                                <td>'.$status.'</td>
                                <td><a onclick="del('.$res['id'].')" class="btn btn-danger btn-xs">移除记录</a></td> </tr>';
                                }
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel-ends">
                <input  type="checkbox" onclick="allcheck(this)" /> 全选　
                <select id="select_exe" class="btn btn-default">
                    <option value="">请选择功能</option>
                    <option value='1'>删除选择的对象</option>
                </select> 　
                <input type="button" value="立即执行" class="btn btn-success" id="btn_exe">　
            </div>
            <?php
            echo'<ul class="pagination">';
            $s = ceil($gls / $pagesize);
            $first=1;
            $prev=$page-1;
            $next=$page+1;
            $last=$s;
            if ($page>1)
            {
                echo '<li><a href="'.$urlfile.'?page='.$first.$link.'">«</a></li>';
                echo '<li><a href="'.$urlfile.'?page='.$prev.$link.'">&laquo;</a></li>';
            } else {
                echo '<li class="disabled"><a>«</a></li>';
                echo '<li class="disabled"><a>&laquo;</a></li>';
            }
            for ($i=1;$i<$page;$i++)
                echo '<li><a href="'.$urlfile.'?page='.$i.$link.'">'.$i .'</a></li>';
            echo '<li class="disabled"><a>'.$page.'</a></li>';
            for ($i=$page+1;$i<=$s;$i++)
                echo '<li><a href="'.$urlfile.'?page='.$i.$link.'">'.$i .'</a></li>';
            echo '';
            if ($page<$s)
            {
                echo '<li><a href="'.$urlfile.'?page='.$next.$link.'">&raquo;</a></li>';
                echo '<li><a href="'.$urlfile.'?page='.$last.$link.'">»</a></li>';
            } else {
                echo '<li class="disabled"><a>&raquo;</a></li>';
                echo '<li class="disabled"><a>»</a></li>';
            }
            echo'</ul>';
            #分页
            ?>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="?act=search" method="POST">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">搜索记录</h4>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" name="pz" placeholder="卡密卷/使用者！">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="submit" class="btn btn-primary">立即搜索</button>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal fade" id="mykamiModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="abstract">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">卡密提取</h4>
                </div>
                <div class="modal-body">
                <select class="form-control" name="type">
                    <option value="1">10分钟内生成的卡密</option>
                    <option value="2">1小时内生成的卡密</option>
                    <option value="3">全部未使用的卡密</option>
                </select><br>
                    <textarea rows="7" cols="" class="form-control" id="kami_list" placeholder="提取的内容"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" onclick="abstract()">立即提取</button>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="?act=add" method="POST">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">生成卡密</h4>
                </div>
                <div class="modal-body">
                    <select class="form-control" name="type">
                        <?php
                            if(isset($config['kami_type'])){
                                if($config['kami_type']){
                                    foreach ($config['kami_type'] as $id => $v){
                                        $v = daddslashes($v);
                                        echo '<option value="'.$id.'">'.$v.'</option>';
                                    }
                                }
                            }
                        ?>
                    </select><br>
                    <input type="number" class="form-control" name="number" id="number" placeholder="生成数量,默认一张卡"><br>
                    <input type="text" class="form-control" value="<?php echo date("Y-m-d H:i:s",strtotime("+1months",strtotime(date('Y-m-d H:i:s',time()))));?>" name="end_time" id="end_time" placeholder="卡密到期时间,默认一个月"><br>
                    <br>
                    <textarea rows="5" cols="" class="form-control" name="remarks" placeholder="卡密备注/活动名称/...用于标记区分卡密的文字,默认是空白"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="submit" class="btn btn-primary">确定生成</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    $(function(){
        $("#btn_exe").click(function(){
            var sindex = $("#select_exe option:selected").val();
            if(sindex == ''){
                layer.msg("操作无效");
                return;
            }
            var str = "";
            $(".chkbox").each(function() {
                if(this.checked == true){
                    if(str != ""){
                        str = str+",";
                    }
                    var e = $(this).attr("ids");
                    str += e;
                }
            });
            if(str == ""){
                layer.msg("无选中数据！");
                return;
            }
            var ii = layer.msg('加载中', {icon: 16,shade: 0.01});
            $.ajax({
                type : "POST",
                url : "ajax.php?mod=kami&act=sp_plexe",
                data : {"str":str,"type":sindex},
                dataType : 'json',
                success : function(data) {
                    layer.close(ii);
                    if(data.code == 1){
                        layer.msg(data.msg);
                        location.reload();

                    }else{
                        layer.msg(data.msg, {icon: 5});
                        return false;
                    }
                },
                error:function(data){
                    layer.close(ii);
                    layer.msg('系统错误！');
                    return false;
                }
            })
        })
        $("#btn-det-ysykm").click(function(){
            if(confirm("确定要删除所有已使用卡密吗？") == false){
                return false;
            }
            var ii = layer.msg('加载中', {icon: 16,shade: 0.01});
            $.ajax({
                type : "POST",
                url : "ajax.php?mod=kami&act=has_del",
                data : {},
                dataType : 'json',
                success : function(data) {
                    layer.close(ii);
                    if(data.code == 1){
                        layer.msg(data.msg);
                        setTimeout(window.location.reload(),3000);
                    }else{
                        layer.msg(data.msg, {icon: 5});
                        return false;
                    }
                },
                error:function(data){
                    layer.close(ii);
                    layer.msg('系统错误！');
                    return false;
                }
            })
        })
        $("#btn-det-allkm").click(function(){
            if(confirm("确定要清空全部卡密吗？") == false){
                return false;
            }
            var ii = layer.msg('加载中', {icon: 16,shade: 0.01});
            $.ajax({
                type : "POST",
                url : "ajax.php?mod=kami&act=all_del",
                data : {},
                dataType : 'json',
                success : function(data) {
                    layer.close(ii);
                    if(data.code == 1){
                        layer.msg(data.msg);
                        setTimeout(window.location.reload(),3000);
                    }else{
                        layer.msg(data.msg, {icon: 5});
                        return false;
                    }
                },
                error:function(data){
                    layer.close(ii);
                    layer.msg('系统错误！');
                    return false;
                }
            })
        })
    })
    function allcheck(e){
        if(e.checked){
            $(".chkbox").each(function() {
                this.checked = true;
            });
        }else{
            $(".chkbox").each(function() {
                this.checked = false;
            });
        }
    }
    function del(id){
        layer.confirm('您正在删除商品是否继续删除？', {
            btn: ['删除','取消'] //按钮
        }, function(){
            var index = layer.msg('加载中', {icon: 16,shade: 0.01});
            $.ajax({
                type: "POST",
                url: "ajax.php?mod=kami&act=del",
                data: {id:id},
                dataType: "json",
                success: function(data){
                    layer.close(index);
                    if(data.code==1){
                        $("#tr_"+id).remove();
                        layer.msg(data.msg, {icon: 1});
                        return false;
                    }else{
                        layer.msg(data.msg, {icon: 5});
                        return false;
                    }
                },
                error: function () {
                    layer.close(index);
                    layer.msg('服务器升级中！');
                    return false;
                }
            })
        }, function(){
        });
    }
    function abstract(){
        var index = layer.msg('加载中', {icon: 16,shade: 0.01});
        $.ajax({
            type: "POST",
            url: "ajax.php?mod=kami&act=abstract",
            data: $("#abstract").serialize(),
            dataType: "json",
            success: function(data){
                layer.close(index);
                if(data.code==1){
                    layer.msg(data.msg, {icon: 1});
                    $.each(data.data, function (key, value) {
                        $("#kami_list").append(value);
                        $("#kami_list").append("\r\n");
                    });
                    return false;
                }else{
                    layer.msg(data.msg, {icon: 5});
                    return false;
                }
            },
            error: function () {
                layer.close(index);
                layer.msg('服务器升级中！');
                return false;
            }
        })
    }
</script>
</body>
</html>

