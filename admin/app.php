<?php
/**
 * Created by Smalls.
 * User: Smalls
 * Email: admin@iiitool.com
 * QQ:13242463,支持定制
 * Date: 2019/1/6
 * Time: 18:25
 */
include("../includes/common.php");
if($islogin==1){}else exit("<script language='javascript'>window.location.href='./login.php';</script>");
$title=' APP配置';
$css = '	<style type="text/css">
		.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
			padding: 15px;
		}
	</style>';
include './common/header.php';
$urlfile = 'app.php';
if(isset($_POST['submit'])) {
    foreach ($_POST as $x => $value) {
        if($x=='admin_pwd')continue;
        $value=daddslashes($value);
        if(empty($conf[$x])){
            $DB->exec("INSERT INTO `smalls_config` (`skey`, `value`) VALUES ('{$x}', '{$value}')");
        }else{
            $DB->query("update `smalls_config` set `value` ='{$value}' where `skey`='{$x}'");
        }
    }
    @header('Content-Type: text/html; charset=UTF-8');
    exit("<script language='javascript'>alert('修改成功！');window.location.href='./".$urlfile."';</script>");
}
?>
<div class="admin-wrap">
    <div class="container">
        <div class="row admin-row">
            <div class="col-sm-offset-2 col-sm-8">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title">系统配置</h3></div>
                    <div class="panel-body">
                        <form action="" method="post" class="form-horizontal" role="form">
                            <h3>APP配置</h3><hr>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">客服QQ</label>
                                <div class="col-sm-9"><input type="number" name="kefu_qq" value="<?php echo $conf['kefu_qq'];?>" class="form-control" required="" placeholder="填写纯数字的QQ号"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">加群Key</label>
                                <div class="col-sm-9"><input type="text" name="qun_key" value="<?php echo $conf['qun_key'];?>" class="form-control" required="" placeholder="多个用小写逗号自己软件做处理,自己软件分割然后循环输出显示等"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">卡密出售地址</label>
                                <div class="col-sm-9"><input type="text" name="kami_carry_url" value="<?php echo $conf['kami_carry_url'];?>" class="form-control" required="" placeholder="就是用户想要购买卡密所打开的网址"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">卡密查询地址</label>
                                <div class="col-sm-9"><input type="text" name="kami_query_url" value="<?php echo $conf['kami_query_url'];?>" class="form-control" required="" placeholder="用户购买完卡密后快捷查询的地址"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">软件停运公告</label>
                                <div class="col-sm-9"><input type="text" name="app_gonggao" value="<?php echo $conf['app_gonggao'];?>" class="form-control" required="" placeholder="停止软件后显示的内容"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">软件状态</label>
                                <div class="col-sm-9">
                                    <select name="app_state" class="form-control">
                                        <option value="1" <?php if($conf['app_state']==1)echo 'selected' ?>>运行中</option>
                                        <option value="0" <?php if($conf['app_state']==0)echo 'selected' ?>>关闭运行</option>
                                    </select>
                                </div>
                                <div class="col-sm-7 col-sm-offset-2" style="margin-top: 5px;">
                                    <div class="alert alert-sm alert-warning" role="alert">
                                        Api接口中,app_state：1:软件运行中,0关闭运行.则整个软件用不了
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">卡密状态</label>
                                <div class="col-sm-9">
                                    <select name="kami_state" class="form-control">
                                        <option value="1" <?php if($conf['kami_state']==1)echo 'selected' ?>>开启卡密激活</option>
                                        <option value="0" <?php if($conf['kami_state']==0)echo 'selected' ?>>关闭卡密激活</option>
                                    </select>
                                </div>
                                <div class="col-sm-7 col-sm-offset-2" style="margin-top: 5px;">
                                    <div class="alert alert-sm alert-warning" role="alert">
                                        kami_state：1:用户可以用卡密激活,0:关闭卡密激活
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">公告状态</label>
                                <div class="col-sm-9">
                                    <select name="gonggao_state" class="form-control">
                                        <option value="1" <?php if($conf['gonggao_state']==1)echo 'selected' ?>>开启公告</option>
                                        <option value="0" <?php if($conf['gonggao_state']==0)echo 'selected' ?>>关闭公告</option>
                                    </select>
                                </div>
                                <div class="col-sm-7 col-sm-offset-2" style="margin-top: 5px;">
                                    <div class="alert alert-sm alert-warning" role="alert">
                                        Api接口中,gonggao_state：1:开启公告,0关闭公告
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">软件公告</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="gonggao" rows="5" placeholder="这里填写公告,换行用换行键就可以了"><?php echo $conf['gonggao'];?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">更新状态</label>
                                <div class="col-sm-9">
                                    <select name="update_state" class="form-control">
                                        <option value="1" <?php if($conf['kami_state']==1)echo 'selected' ?>>强制更新</option>
                                        <option value="2" <?php if($conf['kami_state']==2)echo 'selected' ?>>普通更新</option>
                                    </select>
                                </div>
                                <div class="col-sm-7 col-sm-offset-2" style="margin-top: 5px;">
                                    <div class="alert alert-sm alert-warning" role="alert">
                                        update_state：1:强制更新就是必须要更新,不更新退出软件,2:普通更新不用强制更新
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">下载状态</label>
                                <div class="col-sm-9">
                                    <select name="down_state" class="form-control">
                                        <option value="1" <?php if($conf['kami_state']==1)echo 'selected' ?>>直连下载</option>
                                        <option value="2" <?php if($conf['kami_state']==2)echo 'selected' ?>>网盘下载</option>
                                    </select>
                                </div>
                                <div class="col-sm-7 col-sm-offset-2" style="margin-top: 5px;">
                                    <div class="alert alert-sm alert-warning" role="alert">
                                        down_state：1:直接更新下载,2:调转网盘更新
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">软件下载地址</label>
                                <div class="col-sm-9"><input type="text" name="update_url" value="<?php echo $conf['update_url'];?>" class="form-control" required="" placeholder="软件下载地址"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">软件版本号</label>
                                <div class="col-sm-9"><input type="text" name="update_ver" value="<?php echo $conf['update_ver'];?>" class="form-control" required="" placeholder="大于这个版本号提示更新"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">更新公告</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="update_desc" rows="5" placeholder="更新内容,换行用换行键就可以了"><?php echo $conf['update_desc'];?></textarea>
                                </div>
                            </div>
                            <input type="hidden" name="submit" value="ok">
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-9">
                                    <input type="submit" value="修改保存" class="btn btn-block btn-primary">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
