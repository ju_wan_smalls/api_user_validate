# 网络验证Api用户系统(本系统基于php+mysql开发)

#### 介绍
网络验证用户系统，已经支持Api对接。
后台可以生成卡密、用户管理是否是会员、用户状态。
APP配置，软件开关等等

#### Api架构（POST提交）
注册和登录：http://127.0.0.1/api/user/qzonelogin

1. type跟后台用户的类型配对。例如王者荣耀后台是1的话,那么type也要为1
2. token这个是加密字段，防止恶意提交（这个自己写）
3. timestamp时间戳精确到秒
4. qq，由于这个对接QQ空间登录，那么可以自己获取QQ号提交
5. nickname，用户的昵称
6. mark机型

用户信息：http://127.0.0.1/api/user/info

1. type跟后台用户的类型配对。例如王者荣耀后台是1的话,那么type也要为1
2. token这个是加密字段，防止恶意提交（这个自己写）
3. timestamp时间戳精确到秒
4. qq，由于这个对接QQ空间登录，那么可以自己获取QQ号提交

APP配置：http://127.0.0.1/api/config

1. token这个是加密字段，防止恶意提交（这个自己写）
2. timestamp时间戳精确到秒

#### 安装教程

1. /install/index.php进行安装
2. 后台账号密码smalls

#### 伪静态.htaccess（Apache），nginx这个自己生成

<IfModule mod_rewrite.c>
 RewriteEngine on
 RewriteBase /
 RewriteCond %{REQUEST_FILENAME} !-d
 RewriteCond %{REQUEST_FILENAME} !-f
 ErrorDocument 404 /app/error.php?404
 ErrorDocument 500 /app/error.php?500
 RewriteRule ^api/user/(.*)$ app/user.php?s=$1 [QSA,PT,L]
  RewriteRule ^api/config$ app/config.php [QSA,PT,L]
</IfModule>

#### 参与贡献

1. Smalls(admin@iiitool.com)


#### 小S博客[http://blog.iiitool.com](http://blog.iiitool.com)}

本人承接PHP开发,api接口开发等等,如果有需要可以联系
本系统不保证没有BUG，自己测试如果自己会修复的话自己修复修复（好心的话联系我，我也修复修复）




#### 程序发布

2019.1.7----网络验证用户版本1.0发布

