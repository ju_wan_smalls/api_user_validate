<?php
/**
 * Created by Smalls.
 * User: Smalls
 * Email: admin@iiitool.com
 * QQ:13242463,支持定制
 * Date: 2019/1/6
 * Time: 20:23
 */
header('Content-type: application/json;chartset=uft-8');
header("X-Powered-By:Smalls");
if(isset($_GET['404'])){
    header("HTTP/1.1 404 Not Found");
    header("Status: 404 Not Found");
    $list=[
        'code'=> 404,
        'msg' =>  '404错误,请检查您所寻访的网站',
    ];
    exit(json_encode($list));
}elseif(isset($_GET['500'])){
    header("HTTP/1.1 500 Internal Server Error");
    header("Status: 500 Internal Server Error");
    $list=[
        'code'=> 500,
        'msg' =>  '500错误,请检查您所寻访的网站',
    ];
    exit(json_encode($list));
}