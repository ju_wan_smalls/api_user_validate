<?php
/**
 * Created by Smalls.
 * User: Smalls
 * Email: admin@iiitool.com
 * QQ:13242463,支持定制
 * Date: 2019/1/6
 * Time: 22:14
 */
include("../includes/common.php");

header('Content-type: application/json;chartset=uft-8');
//token用户加密使用的,防止恶意提交
$token = daddslashes($_POST['token']);
//时间戳精确到秒
$timestamp = daddslashes($_POST['timestamp']);

if($timestamp!='' && $token!=''){

    $data = [
        'kefu_qq'=>$conf['kefu_qq'],
        'qun_key'=>$conf['qun_key'],
        'kami_carry_url'=>$conf['kami_carry_url'],
        'kami_query_url'=>$conf['kami_query_url'],
        'gonggao_state'=>$conf['gonggao_state'],
        'down_state'=>$conf['down_state'],
        'update_state'=>$conf['update_state'],
        'app_state'=>$conf['app_state'],
        'kami_state'=>$conf['kami_state'],
        'gonggao_state'=>$conf['gonggao_state'],
        'app_gonggao'=>$conf['app_gonggao'],
        'update_ver'=>$conf['update_ver'],
        'update_desc'=>$conf['update_desc'],
        'update_url'=>$conf['update_url'],
    ];
    show(1,'获取配置成功',$data);
}else{
    show(0,'时间戳或Token输入错误');
}