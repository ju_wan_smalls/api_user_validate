<?php
/**
 * Created by Smalls.
 * User: Smalls
 * Email: admin@iiitool.com
 * QQ:13242463,支持定制
 * Date: 2019/1/6
 * Time: 19:48
 */
include("../includes/common.php");

header('Content-type: application/json;chartset=uft-8');

if(isset($_GET['s'])){
    $s = daddslashes($_GET['s']);
    if($s=='qzonelogin'){
        //token用户加密使用的,防止恶意提交
        $token = daddslashes($_POST['token']);
        //时间戳精确到秒
        $timestamp = daddslashes($_POST['timestamp']);

        //-----------
        $type = daddslashes($_POST['type'])?daddslashes($_POST['type']):1;//类型
        $qq = daddslashes($_POST['qq']);//qq号
        $nickname = daddslashes($_POST['nickname'])?daddslashes($_POST['nickname']):'空白_'.random(5);//昵称
        $mark = daddslashes($_POST['mark'])?daddslashes($_POST['mark']):'MI3';//手机型号
        $user_token = md5($qq.SYS_KEY);
        $update_time = time();
        $add_time = time();
        if($timestamp!='' && $token!=''){
            if(is_numeric($qq)){
                $rows=$DB->query("select * from smalls_user where `qq`='$qq' AND `type`='$type' limit 1")->fetch();
                if($rows){
                    $row=$DB->exec("update `smalls_user` set `update_time` ='{$update_time}' where `qq`='$qq' AND `type`='$type'");
                    $data=[
                        'userid'=>$rows['userid'],
                        'username'=>$rows['username'],
                        'picture'=>$rows['pic'],
                        'token'=>$rows['token'],
                        'nickname'=>$rows['nickname'],
                        'is_vip'=>$rows['is_vip'],
                        'status'=>$rows['status'],
                        'phone'=>$rows['phone'],
                        'type'=>$rows['type'],
                    ];
                    show(1,'登录成功',$data);
                }else{
                    $password = md5($qq);
                    $picture = 'http://qlogo2.store.qq.com/qzone/'.$qq.'/'.$qq.'/100';
                    $ip = real_ip();
                    $row = $DB->exec("INSERT INTO `smalls_user` (`username`, `password`, `pic`, `nickname`,  `qq`, `update_time`, `add_time`, `ip`, `mark`, `type`, `token`) VALUES ('{$qq}', '{$password}', '{$picture}', '{$nickname}', '{$qq}', '{$update_time}', '{$add_time}', '{$ip}', '{$mark}', '{$type}', '{$user_token}')");
                    if($row){
                        $rows=$DB->query("select * from smalls_user where `qq`='$qq' AND `type`='$type' limit 1")->fetch();
                        if($rows){
                            $data=[
                                'userid'=>$rows['userid'],
                                'username'=>$rows['username'],
                                'picture'=>$rows['pic'],
                                'token'=>$rows['token'],
                                'nickname'=>$rows['nickname'],
                                'is_vip'=>$rows['is_vip'],
                                'status'=>$rows['status'],
                                'phone'=>$rows['phone'],
                                'type'=>$rows['type'],
                            ];
                            show(1,'注册成功',$data);
                        }else{
                            show(0,'获取用户内容失败');
                        }
                    }else{
                        show(0,'注册失败,请重新注册');
                    }
                }
            }else{
                show(0,'请输入正确的QQ号');
            }
        }else{
            show(0,'时间戳或Token输入错误');
        }
    }elseif ($s=='info'){
        //token用户加密使用的,防止恶意提交
        $token = daddslashes($_POST['token']);
        //时间戳精确到秒
        $timestamp = daddslashes($_POST['timestamp']);
        //-----------
        $type = daddslashes($_POST['type'])?daddslashes($_POST['type']):1;//类型
        $qq = daddslashes($_POST['qq']);//qq号
        if($timestamp!='' && $token!=''){
            if(is_numeric($qq)){
                $rows=$DB->query("select * from smalls_user where `qq`='$qq' AND `type`='$type' limit 1")->fetch();
                if($rows){
                    $data=[
                        'userid'=>$rows['userid'],
                        'username'=>$rows['username'],
                        'picture'=>$rows['pic'],
                        'token'=>$rows['token'],
                        'nickname'=>$rows['nickname'],
                        'is_vip'=>$rows['is_vip'],
                        'status'=>$rows['status'],
                        'phone'=>$rows['phone'],
                        'type'=>$rows['type'],
                    ];
                    show(1,'获取信息成功',$data);
                }else{
                    show(0,'没有该账户');
                }
            }else{
                show(0,'请输入正确的QQ号');
            }
        }else{
            show(0,'时间戳或Token输入错误');
        }
    }elseif ($s=='addvip'){
        //token用户加密使用的,防止恶意提交
        $token = daddslashes($_POST['token']);
        //时间戳精确到秒
        $timestamp = daddslashes($_POST['timestamp']);
        //-----------
        $type = daddslashes($_POST['type'])?daddslashes($_POST['type']):1;//类型
        $qq = daddslashes($_POST['qq']);//qq号
        $kami = daddslashes($_POST['kami']);//卡密
        if($timestamp!='' && $token!=''){
            if(is_numeric($qq)){
                $rows=$DB->query("select * from smalls_user where `qq`='$qq' AND `type`='$type' limit 1")->fetch();
                if($rows){
                    $kami_db=$DB->query("select * from smalls_kami where `kami`='$kami' limit 1")->fetch();
                    if($kami_db){
                        if($kami_db['status']==1 || $kami_db['userid']!=''){
                            show(0,'该卡密已经被使用啦,请不要重复使用,如果有问题联系客服进行反馈');
                        }
                        if($kami_db['end_time']>time() && time()>$kami_db['add_time']){
                            $is_vip = 1;
                            $row=$DB->exec("update `smalls_user` set `is_vip` ='{$is_vip}' where `qq`='$qq' AND `type`='$type'");
                            if($row){
                                $DB->exec("update `smalls_kami` set `status` ='1',`userid` ='{$rows['userid']}' where `kami`='$kami'");
                                show(1,'成功开通会员',$is_vip);
                            }else{
                                show(0,'开通失败或者你已经是会员');
                            }
                        }else{
                            show(0,'该卡密已经过期');
                        }
                    }else{
                        show(0,'该卡密不存在或者已经被使用啦');
                    }
                }else{
                    show(0,'没有该账户');
                }
            }else{
                show(0,'请输入正确的QQ号');
            }
        }else{
            show(0,'时间戳或Token输入错误');
        }
    }
}else{
    show(0,'文件错误');
}
